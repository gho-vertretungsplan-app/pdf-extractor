import requests

from plantypes import VertPlan

def get_plan(user: str, password: str) -> VertPlan:
    credentials: requests.auth.HTTPBasicAuth = requests.auth.HTTPBasicAuth(user, password)
    plan: VertPlan = VertPlan()

    text: str = requests.get("https://gho.kaidan.im/vertretungsplan.json", auth=credentials).text
    plan.fromJSON(text)

    return plan
