# Server side of the GHO App

Depends on [pdf-table-extract](https://github.com/JBBgameich/pdf-table-extract) in a python3 version.

If you want to setup your own server, just copy this repository to the target folder, most likely `/var/www/html/gho`, and set up a cron job that executes `python3 update.py` in `/var/www/html/gho` at least every two hours. The url of your json files for the app will be `http://your-server.tld/gho/vertretungsplan.json`. To view it from a browser, open `http://your-server.tld/gho/vertretungsplan.html`

## Installing dependencies

```bash
pip3 install -r requirements.txt
```

After that you also need to install poppler. On debian this can be done as follows:

```
sudo apt install poppler-utils
```

