#!/usr/bin/python3
import json
import pdftableextract as pdf
import requests
from requests.auth import HTTPBasicAuth
import sys
import os
import datetime

from plantypes import JsonObject, VertPlanItem, VertPlan

from typing import List, Optional, Any, Union

PDF_URL = "http://www.gho-students.de/VPS.pdf"
PDF_FILE_NAME = "vertretungsplan.pdf"

# download url to given file name; if no name given use name from URL
# chunk size defaults to 1 KiB
def downloadFile(url: str, auth: HTTPBasicAuth, fname: Optional[str] = None, chunkSize: int = 1024) -> str:
    if not fname:
        fname = url.split('/')[-1]

    response = requests.get(url, auth=auth, stream=True)

    # download each chunk and save to file
    with open(fname, 'wb') as f:
        for chunk in response.iter_content(chunk_size=chunkSize):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
    # return the file name (only useful, if name was get from url)
    return fname

def extractCellsFromPdf(fname: str, firstPage: int = 2) -> List[List[Any]]:
    cells: List[Any] = []
    pages: List[int] = []
    currentPage: int = firstPage

    while True:
        try:
            cells.append(pdf.process_page(PDF_FILE_NAME, str(currentPage)))

            pages.append(currentPage)
            currentPage += 1
        except:
            print("Found {} page(s)".format(currentPage - firstPage))
            break

    # flatten the cells structure
    cells = [item for sublist in cells for item in sublist]
    pageCells: List[List[Any]] = []
    for page in pages:
        pageCells.append(pdf.table_to_list(cells, pages)[page])

    return pageCells

CELL_READ_INDEX: int = -1
def readCell(row: List[str], reset: bool = False) -> str:
    if reset:
        global CELL_READ_INDEX
        CELL_READ_INDEX = -1

    CELL_READ_INDEX += 1

    value: str = row[CELL_READ_INDEX].strip()

    if value == "---":
        value = ""

    return value


def convertCellsToJson(pageRows: List[List[Any]], dlTime: datetime.datetime) -> str:
    json = VertPlan()
    json.title = pageRows[0][1][0].replace("Klasse - Erweitert ", "")
    json.lastUpdated = dlTime.strftime("%Y-%m-%dT%H:%M")
    json.data = []

    for rowsList in pageRows:
        coursesList = rowsList[2][0].replace("betroffen Klasse(n)", "").strip().split(" ")

        rowsList = rowsList[3:]

        # ignore row if has not enough fields / all fields are empty
        # ['', 'Ma E11 ', '3 ', 'MülS ', 'Most ', 'Ku ', 'B26 ', 'Betreuung ', '']
        rowsList = [row for row in rowsList if len(row) >= 9 and row[2:] != row[:-2]]

        for i in range(0, len(rowsList)):
            row = rowsList[i]

            item = VertPlanItem()
            # courses are currently not parsed correctly, this is a hack
            readCell(row, reset = True)
            try:
                item.course = coursesList[i]
            except IndexError:
                item.course = ""
            item.regularSubject = readCell(row)
            item.lesson = readCell(row)
            item.regularTeacher = readCell(row)
            item.substituteTeacher = readCell(row)
            item.substituteSubject = readCell(row)
            item.substituteRoom = readCell(row)
            item.type = readCell(row)
            #item.comment = readCell(row)
            #item.regularRoom = readCell(row)

            json.data.append(item)

    json.numberOfPages = len(pageRows)

    return json.toJSON()

def main(username: str, password: str) -> None:
    # get current time needed later for 'lastUpdated' in JSON
    dlTime: datetime.datetime = datetime.datetime.now()
    # download Vertretungsplan pdf
    downloadFile(PDF_URL, HTTPBasicAuth(username, password), fname = PDF_FILE_NAME)
    print("Downloading took {}".format(datetime.datetime.now() - dlTime))
    # extract JSON from pdf table
    cells = extractCellsFromPdf(PDF_FILE_NAME)
    # delete pdf file
    os.remove(PDF_FILE_NAME)
    # build json
    json = convertCellsToJson(cells, dlTime)
    # Save json
    file = open("vertretungsplan.json", "w")
    file.write(json)

if __name__ == "__main__":
    # arguments
    if len(sys.argv) < 3:
        print("No username or password given!")
        print("Usage: " + sys.argv[0] + " USERNAME PASSWORD")
        exit(1)

    main(sys.argv[1], sys.argv[2])
