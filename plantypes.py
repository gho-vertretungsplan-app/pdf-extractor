import json

from typing import List, Dict, Any

class JsonObject:
    def toJSON(self) -> str:
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)

class VertPlanItem(JsonObject):
    course: str
    lesson: str
    regularTeacher: str
    substituteTeacher: str
    substituteSubject: str
    substituteRoom: str
    type: str
    comment: str
    regularSubject: str
    regularRoom: str

    def __str__(self) -> str:
        return self.__dict__.__str__()

class VertPlan(JsonObject):
    title: str
    lastUpdated: str
    data: List[VertPlanItem]
    numberOfPages: int

    def __str__(self) -> str:
        return self.__dict__.__str__()

    def fromJSON(self, json_data: str) -> None:
        json_dict: Dict[str, Any] = json.loads(json_data)
        # Deserialize first level of data
        self.__dict__ = json_dict.copy()

        # Reset data list, we want it to contain properly typed data
        self.data = []
        for obj in json_dict["data"]:
            item: VertPlanItem = VertPlanItem()
            item.__dict__ = obj
            self.data.append(item)
