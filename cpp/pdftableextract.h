#ifndef PDFTABLEEXTRACT_H
#define PDFTABLEEXTRACT_H

#include <pybind11/embed.h>

#include <vector>

namespace py = pybind11;

namespace pdftableextract {
py::list process_page(const std::string &fileName, const std::string pages) {
    return py::module::import("pdftableextract").attr("process_page")(fileName, pages).cast<py::list>();
}

/**
 * Output list of lists
 */
py::list table_to_list(py::list cells) {
    return py::module::import("pdftableextract").attr("table_to_list")(cells, py::list()).cast<py::list>();
}
}

#endif // PDFTABLEEXTRACT_H
