#ifndef REQUESTS_H
#define REQUESTS_H

#include <pybind11/embed.h>

namespace py = pybind11;
using namespace pybind11::literals; // to bring in the `_a` literal

namespace requests {
class PyWrapper {
public:
    py::object py_object() {
        return m_object;
    }

protected:
    py::object m_object;
};

namespace auth {

class HTTPBasicAuth : public PyWrapper {
public:
    HTTPBasicAuth (const std::string &user, const std::string &password)
        : PyWrapper()
    {
        m_object = py::module::import("requests").attr("auth").attr("HTTPBasicAuth")(user, password);
    }
};

}

class Response : public PyWrapper {
public:
    Response(const py::object &response) {
        m_object = response;
    }
    std::string apparent_encoding() const {
        return m_object.attr("apparent_encoding").cast<std::string>();
    }
    void close() {
        m_object.attr("close")();
    }
    std::string content() const {
        return m_object.attr("content").cast<std::string>();
    }
    // TODO Wrap
    py::object cookies() const {
        return m_object.attr("cookies");
    }
    py::object elapsed() const {
        return m_object.attr("elapsed");
    }
    std::string encoding() const {
        return m_object.attr("encoding").cast<std::string>();
    }
    py::dict headers() const {
        return m_object.attr("headers").cast<py::dict>();
    }
    std::vector<Response> history() const {
        const py::list pyhistory = m_object.attr("history").cast<py::list>();
        pyhistory.begin();

        std::vector<Response> history;
        for (const auto &object : pyhistory) {
            history.push_back(Response(object.cast<py::object>()));
        }

        return history;
    }
    bool is_permanent_redirect() const {
        return m_object.attr("is_permanent_redirect").cast<bool>();
    }
    bool is_redirect() const {
        return m_object.attr("is_redirect").cast<bool>();
    }

    // TOOD more
};

Response get(const std::string &url, std::optional<auth::HTTPBasicAuth> auth = std::nullopt) {
    py::object response;

    if (auth.has_value()) {
        response = py::module::import("requests").attr("get")(url, "auth"_a=*auth->py_object());
    } else {
        response = py::module::import("requests").attr("get")(url);
    }

    return Response(response);
}

}

#endif // REQUESTS_H
