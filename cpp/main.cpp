#include <iostream>

#include <pybind11/embed.h>

#include <QFile>
#include <QTime>
#include <QCommandLineParser>

#include "json.hpp"

#include "requests.h"
#include "pdftableextract.h"

namespace py = pybind11;

constexpr auto PDF_URL = "http://gho-students.de/VPS.pdf";
constexpr auto PDF_FILE_NAME = "vertretungsplan.pdf";


bool downloadFile(const std::string &url, const std::string &user, const std::string &password, const std::string &fileName) {
    const auto auth = requests::auth::HTTPBasicAuth(user, password);
    const requests::Response response = requests::get(url, auth);
    const std::string content = response.content();

    QFile file(QString::fromStdString(fileName));
    if (!file.open(QIODevice::WriteOnly)) {
        std::cout << "Failed to write downloaded file";
        return false;
    }
    file.write(QByteArray::fromStdString(content));

    return true;
};

py::list extractCellsFromPdf(const std::string &fileName, int firstPage = 2) {
    const auto pdf = py::module::import("pdftableextract");

    py::list cells;
    std::vector<int> pages;
    int currentPage = firstPage;

    while (true) {
        try {
            const auto list = pdftableextract::process_page(fileName, std::to_string(currentPage));
            cells.append(list);

            pages.push_back(currentPage);
            currentPage++;
        } catch (const py::error_already_set &error) {
            py::print("Found", std::to_string(currentPage), "page(s)");
            break;
        }
    }

    py::list flat_cells;
    // flatten the cells structure
    for (const auto &sublist : cells) {
        for (const auto &item : sublist) {
            flat_cells.append(item);
        }
    }

    py::list page_cells;

    const auto table_to_list = pdftableextract::table_to_list(flat_cells);

    for (const auto page : pages) {
        page_cells.append(table_to_list[page]);
    }

    return page_cells;
}

int CELL_READ_INDEX = -1;

std::string readCell(const py::list &row, bool reset = false) {
    if (reset) {
        CELL_READ_INDEX = -1;
    }

    CELL_READ_INDEX++;

    std::string value = row[CELL_READ_INDEX].attr("strip")().cast<std::string>();

    if (value == "---") {
        value = "";
    }

    return value;
}

std::string convertCellsToJson(py::list pageRows, const QTime &dltime) {
    const auto title = pageRows[0].cast<py::list>()[1].cast<py::list>()[0].attr("replace")("Klasse - Erweitert ", "").attr("strip")().cast<std::string>();

    std::vector<nlohmann::json> dataList;
    for (const auto &object : pageRows) {
        const auto rowsList = object.cast<py::list>();
        const auto coursesList = rowsList[2].cast<py::list>()[0].attr("replace")("betroffene Klasse(n)", "").attr("strip")().attr("split")(" ").cast<py::list>();

        py::list relevantRows;
        for (pybind11::size_t i = 3; i < rowsList.size() ; i++) {
            relevantRows.append(rowsList[i]);
        }

        py::list filteredRows;
        //ignore row if has not enough fields / all fields are empty
        for (const auto &row : relevantRows) {
            if (py::len(row) > 9 && !std::all_of(row.begin(), row.end(), [](const py::handle &item) {
                return item.cast<std::string>().empty();
            })) {
                filteredRows.append(row);
            }
        }

        for (pybind11::size_t i = 0; i < filteredRows.size(); i++) {
            const auto selectedRow = filteredRows[i];

            nlohmann::json item;
            readCell(selectedRow, true);
            try {
                item["course"] = coursesList[i].cast<std::string>();
            } catch (const py::error_already_set &error) {
                item["course"] = "";
            }

            item["regularSubject"] = readCell(selectedRow);
            item["lesson"] = readCell(selectedRow);
            item["regularTeacher"] = readCell(selectedRow);
            item["substituteTeacher"] = readCell(selectedRow);
            item["substituteSubject"] = readCell(selectedRow);
            item["substituteRoom"] = readCell(selectedRow);
            item["type"] = readCell(selectedRow);
            item["comment"] = readCell(selectedRow);
            item["regularRoom"] = readCell(selectedRow);

            dataList.push_back(item);
        }
    }

    nlohmann::json json;
    json["title"] = title;
    json["lastUpdated"] = dltime.toString().toStdString();
    json["data"] = dataList;
    json["numberOfPages"] = py::len(pageRows);

    return json.dump(4);
}

int main(int argc, char *argv[]) {
    py::scoped_interpreter guard {}; // start the interpreter and keep it alive
    const auto dlTime = QTime::currentTime();

    QCoreApplication app(argc, argv);
    QCommandLineParser parser;

    QCommandLineOption userOption("user", "Username for authentication", "user");
    QCommandLineOption passwordOption("password", "Password for authentication", "password");
    parser.addOption(userOption);
    parser.addOption(passwordOption);
    parser.process(app);

    if (!parser.isSet(userOption) || !parser.isSet(passwordOption)) {
        std::cout << "Required argument user or password missing";
        parser.showHelp();
    }

    const auto user = parser.value(userOption).toStdString();
    const auto password = parser.value(passwordOption).toStdString();

    downloadFile(PDF_URL, user, password, PDF_FILE_NAME);
    const auto cells = extractCellsFromPdf(PDF_FILE_NAME);
    const auto json = convertCellsToJson(cells, dlTime);

    QFile file("vertretungsplan.json");
    if (!file.open(QIODevice::WriteOnly)) {
        std::cout << "Could not open output file for writing";
    }

    file.write(QByteArray::fromStdString(json));
}
